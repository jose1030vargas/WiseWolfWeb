export const languages = {
    en:{
        home: 'Home',
        about_us: 'About us',
        become_scholar: 'be a scholar',
        contact_us: 'Contact Us',
        login: 'Login',
        title_aboutus: 'About us',
        title_mision: '¿What is our mission?',
        text_aboutus: 'WiseWolf Academy was born on July 12, 2021 with the intention of giving economic freedom to people around the world.',
        text_mision: 'To contribuite with the development and economic freedom for people who are looking to get involve with the blockchain gaming and NFT world. We offer scholarships programs, advisory, education in crypto currencies for a well compliance of your goals.',
        title_form: 'Apply for Scholarship',
        descrip_form: 'Apply to one of our Scholarships',
        form_name:'Names',
        form_country: 'Country',
        form_phone: '(Area code) Phone',
        form_ronin: 'Ronin address',
        form_lastname: 'Last Names',
        form_email: 'Email',
        form_occupation: 'Occupation',
        form_submit:'Submit',
        form_comment: 'Tell us why you want to be part of the pack',
        selec_lang: 'Select your languages',
        input_require: 'This field is required',
        email_error: 'It must be a valid Email Ex: ex@ex.ex',
        phone_error: 'The phone must have between 6 and 17 characters plus the area code',
        ronin_error: 'It should follow the format ronin:xxxxxx',
        success_submit: 'Your request has been submitted successfully',
        title_ourscholars: 'Our Scholars',
        footer_pack: 'Join the Pack',
        footer_socialNetw: 'Social Networks',
    },
    es:{
        home: 'Inicio',
        about_us: 'Sobre nosotros',
        become_scholar: 'Aplica a nuestras becas',
        contact_us: 'Contactanos',
        login: 'Login',
        title_aboutus: '¿Quienes somos?',
        title_mision: '¿Cual es nuestra misión?',
        text_aboutus: 'WiseWolf Academia nace un 12 de Julio de 2021 con la intención de dar independencia económica a personas alrededor del mundo. ',
        text_mision: 'Contribuir con el crecimiento e independencia económica para personas que deseen involucrarse en el mundo de juegos blockchain y NFT brindando prestaciones de becas, asesoría, educación y seguimiento en el cumplimiento de metas y objetivos propuestos',
        title_form : 'Solicita una Beca',
        descrip_form: 'Aplica a una de nuestras Becas',
        form_name:'Nombres',
        form_country: 'País',
        form_phone: '(Codigo de area)Telefono',
        form_ronin:'Dirección de Ronin',
        form_lastname: 'Apellidos',
        form_email: 'Email',
        form_occupation: 'Ocupación',
        form_submit:'Enviar',
        form_comment: 'Cuentanos por qué quieres ser parte de la manada',
        selec_lang: 'Seleccióne el idioma',
        input_require: 'Este campo es obligatorio',
        email_error: 'Debe ser un Email valido Ej: ej@ej.ej',
        phone_error: 'El telefono debe tener entre 6 y 17 caracteres mas el codigo de area',
        ronin_error: 'Debe seguir el formato ronin:xxxxxx',
        success_submit: 'Se ha enviado su solicitud con éxito',
        title_ourscholars: 'Nuestros Becados',
        footer_pack: 'Unete a la Manada',
        footer_socialNetw: 'Redes Sociales',
    }
}



export const languageScholar = {
    en: {
        scholar1_text: `Estoy muy agradecido con WiseWolf Academy por la oportunidad brindada y el apoyo de todos sus miembros , no solo dan becas con buenos axies sino que también brindan servicio de coach para ir mejorando en la arena. En los meses que estuve como becado fue muy satisfactorio y agradable el ambiente.
                        Gracias al apoyo económico y en la experiencia obetinda con la beca ya tengo mi propio equipo de axies y espero poder aportar mi granito de arena para el crecimiento de la academia.`,
        scholar1_range: 'Graduate',
        scholar1_name: 'Grabiel Ojeda',

        scholar2_text: `La mejor academia. Mi experiencia en la academia ha sido extraordinaria. Durante todo mi trayecto he recibido un apoyo increíble por parte de los Coach y el Director Ejecutivo, siempre están ahí para apoyarte si tienes dudas o algún inconveniente. Las exigencias de la academia son fáciles de cumplir, tienen actividades extra para motivar a todos los integrantes y si te esfuerzas al máximo siempre tendrás una recompensa.`,
        scholar2_range: 'Scholar',
        scholar2_name: 'Mayi Zapata',

        scholar3_text: `Cuando recien comenzas el jardin de niños contas con apoyo de las maestras y tus compañeritos para jugar y salir adelante. Las maestras te enseñan, aconsejan y te hacen saber que para cualquier duda o pregunta estan para vos. WiseWolf es bastante parecido dentro de estos terminos. Ni bien entre a la academia en mi caso se sintio de esa manera, me dieron la información necesaria (siempre de manera muy organizada), ofrecen todo el tiempo servicio de coaching y ademas de apoyarnos como comunidad. Tambien cuentan con cosas aparte del trabajo para divertirnos juntos.`,
        scholar3_range: 'Scholar',
        scholar3_name: 'Crimi',

        scholar4_text: `Más que una academia, WiseWolf es una casa, donde su directiva son la cabeza que se encargan de brindar buena guía a sus becados y ayudarlos en la busca de un mejor futuro.`,
        scholar4_range: 'Scholar',
        scholar4_name: 'Jose Cisneros',

        scholar5_text: `Hola a todos, soy uno de los miembros nuevos de WiseWolf Academy, en el poco tiempo compartido he podido determinar la seriedad, compañerismo y dedicación del equipo administrativo, directivo y cada uno de los que formamos parte de este gran proyecto, son muchas personas de diferentes países que cada día dan lo mejor de sí en cada uno de sus funciones. He aprendido mucho y espero seguir recibiendo enseñanzas.`,
        scholar5_range: 'Scholar',
        scholar5_name: 'Marié González',

        scholar6_text: `Hola a todos, para mí es un honor pertenecer a Wisewolf Academy, son amables, responsables, educados y sobre todo muy profesionales, su seriedad y preocupación por los miembros que la integran lo hacen sentir a uno cómodo y seguro, estoy muy agradecida por dejarme formar parte de esta comunidad, gracias.`,
        scholar6_range: 'Scholar',
        scholar6_name: 'Sassenach',

        scholar7_text: `Wolf academy es una oportunidad única, te ayuda a explorar el mundo de Axie Infinity, te reciben con una calidez y compañerismo para el crecimiento en Axie, donde aprendes a trabajar en equipo, y se ayudan unos con otros para mejorar tus estrategias y poder ser mas competitivo, estoy muy agradecido con la oportunidad de entrar a este desafío.`,
        scholar7_range: 'Scholar',
        scholar7_name: 'Hugo Zamora',
    },

    es:{
        scholar1_text: `Estoy muy agradecido con la WiseWolf Academy por la oportunidad brindada y el apoyo de todos sus miembros , no solo dan becas con buenos axies sino que también brindan servicio de coach para ir mejorando en la arena. En los meses que estuve como becado fue muy satisfactorio y agradable el ambiente.
                        Gracias al apoyo económico y en la experiencia obetinda con la beca ya tengo mi propio equipo de axies y espero poder aportar mi granito de arena para el crecimiento de la academia.`,
        scholar1_range: 'Graduado',
        scholar1_name: 'Grabiel Ojeda',

        scholar2_text: `La mejor academia. Mi experiencia en la academia ha sido extraordinaria. Durante todo mi trayecto he recibido un apoyo increíble por parte de los Coach y el Director Ejecutivo, siempre están ahí para apoyarte si tienes dudas o algún inconveniente. Las exigencias de la academia son fáciles de cumplir, tienen actividades extra para motivar a todos los integrantes y si te esfuerzas al máximo siempre tendrás una recompensa.`,
        scholar2_range: 'Becada',
        scholar2_name: 'Mayi Zapata',

        scholar3_text: `Cuando recien comenzas el jardin de niños contas con apoyo de las maestras y tus compañeritos para jugar y salir adelante. Las maestras te enseñan, aconsejan y te hacen saber que para cualquier duda o pregunta estan para vos. WiseWolf es bastante parecido dentro de estos terminos. Ni bien entre a la academia en mi caso se sintio de esa manera, me dieron la información necesaria (siempre de manera muy organizada), ofrecen todo el tiempo servicio de coaching y ademas de apoyarnos como comunidad. Tambien cuentan con cosas aparte del trabajo para divertirnos juntos.`,
        scholar3_range: 'Becada',
        scholar3_name: 'Crimi',

        scholar4_text: `Más que una academia, WiseWolf es una casa, donde su directiva son la cabeza que se encargan de brindar buena guía a sus becados y ayudarlos en la busca de un mejor futuro.`,
        scholar4_range: 'Becado',
        scholar4_name: 'Jose Cisneros',

        scholar5_text: `Hola a todos, soy uno de los miembros nuevos de WiseWolf Academy, en el poco tiempo compartido he podido determinar la seriedad, compañerismo y dedicación del equipo administrativo, directivo y cada uno de los que formamos parte de este gran proyecto, son muchas personas de diferentes países que cada día dan lo mejor de sí en cada uno de sus funciones. He aprendido mucho y espero seguir recibiendo enseñanzas.`,
        scholar5_range: 'Becado',
        scholar5_name: 'Marié González',

        scholar6_text: `Hola a todos, para mí es un honor pertenecer a Wisewolf Academy, son amables, responsables, educados y sobre todo muy profesionales, su seriedad y preocupación por los miembros que la integran lo hacen sentir a uno cómodo y seguro, estoy muy agradecida por dejarme formar parte de esta comunidad, gracias.`,
        scholar6_range: 'Scholar',
        scholar6_name: 'Sassenach',

        scholar7_text: `Wolf academy es una oportunidad única, te ayuda a explorar el mundo de Axie Infinity, te reciben con una calidez y compañerismo para el crecimiento en Axie, donde aprendes a trabajar en equipo, y se ayudan unos con otros para mejorar tus estrategias y poder ser mas competitivo, estoy muy agradecido con la oportunidad de entrar a este desafío.`,
        scholar7_range: 'Scholar',
        scholar7_name: 'Hugo Zamora',


    }
}