import './App.css';
import Layout from '../components/layout'
import {routes} from './allRoutes'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import LangState from '../context/languages/LangState';

function App() {
  
  return (
    <>
    <LangState>
      <Router>
        <Layout>
          <Switch>
            
            {routes.map((route,index)=>
              
                <Route path={route.path} component={route.component} key={index}/>
              
            )}
            
          </Switch>
        </Layout>
      </Router>
    </LangState>
      
    </>
  );
}

export default App;
