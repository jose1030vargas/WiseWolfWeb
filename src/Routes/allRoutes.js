import Home from '../views/Home'
import Login from '../views/Login'

export const routes = [
    {
        path: '/Login',
        component: Login
    },
    {
        path: '/',
        component: Home
    },
]
   
