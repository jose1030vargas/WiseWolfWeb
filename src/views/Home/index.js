import React from 'react'
import AboutUs from '../../components/aboutus'
import Banner from '../../components/banner'
import Form from '../../components/form'
import FirstBanner from '../../components/firstBanner'
import OurSchorlars from '../../components/ourScholars'
import './index.css'
import { ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Home = () => {
    return (
        <div className="App">
            <div className="cont-app">
                <FirstBanner/>
                <AboutUs/>
                <Banner type={1} />
                <Form/>
                <Banner type={2} />
                <OurSchorlars />
            </div>
            <ToastContainer />
        </div>
    )
}

export default Home
