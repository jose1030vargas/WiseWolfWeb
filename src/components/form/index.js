import {useContext} from 'react'
import InputsForm from './inputsForm'
import LangContext from '../../context/languages/LangContext'
import {languages} from '../../languages/languages'
import ImgForm from '../../assets/formImg.png'
import './index.css'

const Form = () => {
    const {language} = useContext(LangContext);
    return (
        <div id='cont-form' className='cont-form'>
            <div className='cont-form-inputs'>
                <div className='cont-form-titles'>
                    <div className='cont-form-titlesCont'>
                        <h2>{languages[language].title_form}</h2>
                        {/*<h4>{languages[language].descrip_form}</h4>*/}
                    </div>
                    <img src={ImgForm} alt='axie form' />
                </div>
                
                    <InputsForm />
                

            </div>
        </div>
    )
}

export default Form
