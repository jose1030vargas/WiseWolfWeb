const validateData = (data)=>{
    let bool = false;
    Object.keys(data).forEach( dat =>{
        
        if(data[dat] === '' && dat !== 'obser'){
            
            bool = true;
            return;
        }
    })
    return bool;
}

const validateEmail = (data) =>{
    if (
        !/^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i.test(
            data
        ) &&
        data.length !== 0
      ) {
        return true;
      } else {
        return false;
      }
}

const validatePhone = (data) =>{
    if ((data.length < 6 || data.length > 16) && data.length !== 0) {
        return true;
      } else {
        return false;
      }
}

const validateRonin = (data) =>{
    if(data !== '' && !data.includes('ronin:')){
        return true;
    }else{
        return false;
    }
}

export {
    validateData,
    validateEmail,
    validatePhone,
    validateRonin,

}