import {useContext, useState} from 'react'
import './index.css'
import {languages} from '../../../languages/languages'
import LangContext from '../../../context/languages/LangContext'
import Slp from '../../../assets/slp.png'
import {
    validateData,
    validateEmail,
    validatePhone,
    validateRonin
} from './validateData'
import { notify } from '../../notify'


const InputsForm = () => {
    const [values, setValues] = useState({

        name: '',
        nationality: '',
        phone: '',
        ronin: '',
        lastname: '',
        email:'',
        ocuppat: '',
        obser: '',

    });

    

    const [error,setError] = useState(false);

    const {language} = useContext(LangContext);

    const onChange = (e)=>{
        setValues({
            ...values,
            [e.target.name]: e.target.value
        })
    }

    const onSubmit = ()=>{
        console.log(values)
        if(validateData(values)){
            
            setError(true);
            return;
        }

        if(validateEmail(values.email)){
            
            return;
        }
        if(validatePhone(values.phone)){
            
            return;
        }
        if(validateRonin(values.ronin)){
            
            return;
        }
        setError(false);
        console.log(values)
        notify(language && languages[language].success_submit,'success')
    }

    return (
        <div className='form-inputs-cont'>
            <div className='form-inputs-izq'>
                
                <div className='cont-input-label'>
                    <label >{language && languages[language].form_name}<span className='input-span'>*</span></label>
                    <input className='inputsForm' value={values.name} name='name' onChange={(e)=>onChange(e)} />
                    <span className='span-erro-form'>{values.name === '' && error && language && languages[language].input_require }</span>
                </div>
               
                <div className='cont-input-label'>
                    <label >{language && languages[language].form_country}<span className='input-span'>*</span></label>
                    <input className='inputsForm' value={values.nationality} name='nationality' onChange={(e)=>onChange(e)} />
                    <span className='span-erro-form'>{values.nationality === '' && error && language && languages[language].input_require }</span>
                </div>
                <div className='cont-input-label'>
                    <label >{language && languages[language].form_phone}<span className='input-span'>*</span></label>
                    <input className='inputsForm' value={values.phone} name='phone' onChange={(e)=>onChange(e)} />
                    <span className='span-erro-form'>{values.phone === '' && error && language && languages[language].input_require }</span>
                    <span className='span-erro-form'>{values.phone !== '' && validatePhone(values.phone)  && language && languages[language].phone_error }</span>
                </div>
                <div className='cont-input-label'>
                    <label >{language && languages[language].form_ronin}<span className='input-span'>*</span></label>
                    <input className='inputsForm' value={values.ronin} name='ronin' onChange={(e)=>onChange(e)} />
                    <span className='span-erro-form'>{values.ronin === '' && error && language && languages[language].input_require }</span>
                    <span className='span-erro-form'>{values.ronin !== '' && validateRonin(values.ronin)  && language && languages[language].ronin_error }</span>
                </div>
            </div>
            <div className='form-inputs-der'>

                <div className='cont-input-label'>
                    <label >{language && languages[language].form_lastname}<span className='input-span'>*</span></label>
                    <input className='inputsForm' value={values.lastname} name='lastname' onChange={(e)=>onChange(e)} />
                    <span className='span-erro-form'>{values.lastname === '' && error && language && languages[language].input_require }</span>
                </div>
                <div className='cont-input-label'>
                    <label >{language && languages[language].form_email}<span className='input-span'>*</span></label>
                    <input className='inputsForm' value={values.email} name='email' onChange={(e)=>onChange(e)} />
                    <span className='span-erro-form'>{values.email === '' && error && language && languages[language].input_require }</span>
                    <span className='span-erro-form'>{values.email !== '' && validateEmail(values.email)  && language && languages[language].email_error }</span>
                </div>
                <div className='cont-input-label'>
                    <label >{language && languages[language].form_occupation}<span className='input-span'>*</span></label>
                    <input className='inputsForm' value={values.ocuppat} name='ocuppat' onChange={(e)=>onChange(e)} />
                    <span className='span-erro-form'>{values.ocuppat === '' && error && language && languages[language].input_require }</span>
                </div>
                
            </div>
            <div className='form-botton'>
                <div className='form-botton-text'>
                <label>{language && languages[language].form_comment}</label>
                <textarea className='textForm' value={values.obser} name='obser' onChange={(e)=>onChange(e)} />
                </div>
                <div className='form-botton-button'>
                    <button className='button-form' onClick={()=>onSubmit()}>
                        {language && languages[language].form_submit}<img src={Slp} height='24px' alt='slp'
                        
                        />
                    </button>
                </div>
            </div>
        </div>
    )
}

export default InputsForm
