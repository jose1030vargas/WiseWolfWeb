import React from 'react'
import Video from '../../assets/wolf.mp4'
import ReactPlayer from 'react-player'
import './index.css'

const FirstBanner = () => {
    return (
        <div id='const-space' className='const-space'>
             <ReactPlayer
                url={Video}
                className='react-player'
                playing
                width='100%'
                height='100%'
                muted
                loop
            />
        </div>
    )
}

export default FirstBanner
