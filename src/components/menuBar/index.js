import {useContext,useState} from 'react'
import {languages} from '../../languages/languages'
import './index.css'
import WiseLogo from '../../assets/WiseWolf_PNG_web.png'
import {Link} from 'react-router-dom'
import LangContext from '../../context/languages/LangContext'
import SpainFlag from '../../assets/spainflag.png'
import UsaFlag from '../../assets/usaflag.png'


const MenuBar = () => {
    const {language,dispatch} = useContext(LangContext);
    const [toggle,setToggle] = useState(false);
    const [screen] = useState(window.screen.width)
  
    return (
        <>{ screen > 600
            ?
            (
                <div id='menubar-cont' className='menubar-cont'>

                    <div className='menubar-contLogo'>
                        <Link to='/'><img src={WiseLogo} className='logo-wise' alt='Logo WiseWolf' /></Link>
                    </div>

                    <nav className='menubar-contUl'>
                        <ul className='menubar-ul'>
                            <li >
                                <a href='#about-cont' >
                                    <h3>{language && languages[language].about_us}</h3>
                                </a>
                            </li>

                            <li >
                                <a href='#cont-form'  >
                                    <h3>{language && languages[language].become_scholar}</h3>
                                </a>
                            </li>

                            <li>
                                <h3>{language && languages[language].contact_us}</h3>
                            </li>
                            <li>
                                <Link to='/Login'><h3>{language && languages[language].login}</h3></Link>
                            </li>
                            <li className='tooltip'>
                            
                                <button
                                    className={`togglebutton ${language === 'en' && 'espLang'}`}
                                    onClick={() => {
                                        dispatch({type:'CHANGE_LANG',payload: language === 'en' ? 'es' : 'en' })
                                    }}
                                ><img className='toggleFlag' src={language === 'en' ? SpainFlag : UsaFlag} alt='flag' /></button>
                                <span className="tooltiptext">{language && languages[language].selec_lang}</span>
                            </li>
                        </ul>
                    </nav>
                </div>
            )
            :
            (
                <>
                <div className={`hamburger ${toggle ? 'is-active' : ''}`} onClick={()=>setToggle(!toggle)}>
                    <div className="_layer -top"></div>
                    <div className="_layer -mid"></div>
                    <div className="_layer -bottom"></div>
                </div>
                <nav className={`menuppal ${toggle ? 'is_active' : ''}`} >
                <ul >
                            <li onClick={()=>setToggle(!toggle)}>
                                <a href='#about-cont' ><h3>{language && languages[language].about_us}</h3></a>
                            </li>
                            <li onClick={()=>setToggle(!toggle)}>
                                <a href='#cont-form' ><h3>{language && languages[language].become_scholar}</h3></a>
                            </li>
                            <li>
                                <h3>{language && languages[language].contact_us}</h3>
                            </li>
                            <li>
                                <Link to='/Login'><h3>{language && languages[language].login}</h3></Link>
                            </li>
                            <li className='tooltip li-new-menu'>
                            
                                <button
                                    className={`togglebutton ${language === 'en' && 'espLang'}`}
                                    onClick={() => {
                                        dispatch({type:'CHANGE_LANG',payload: language === 'en' ? 'es' : 'en' })
                                    }}
                                ><img className='toggleFlag' src={language === 'en' ? SpainFlag : UsaFlag} alt='flag' /></button>
                                <span className="tooltiptext new-tooltip">{language && languages[language].selec_lang}</span>
                            </li>
                        </ul>
                </nav>
                </>
            )
        }
        
        </>
    )
}

export default MenuBar
