import {useState} from 'react'
import { BsArrowUpShort } from "react-icons/bs";
import './index.css'

const ButtonTop = () => {
    const [screen] = useState(600 ||window.screen.width)
    return (
        <button className='home-button-top'>
            <a href={`${screen > 600 ? '#menubar-cont' : '#const-space'}`}>
                <BsArrowUpShort className='home-button-top-img' />
            </a>
        </button>
    )
}

export default ButtonTop
