import {useContext} from 'react'
import {languages} from '../../languages/languages'
import LangContext from '../../context/languages/LangContext'
import Card from '../card'
import useInfoCard from '../../hooks/infoCard/useInfoCard'
import './index.css'

const OurSchorlars = () => {
    const {language} = useContext(LangContext);
    const scholars = useInfoCard(language);

    

    return (
        <div id='cont-outScho' className='cont-outScho'>
            <div className='outSho-title-cards'>
                <h2>{language && languages[language].title_ourscholars}</h2>

                <div className='cont-outs-card'>
                    {scholars.map((scholar,index)=>{
                        
                        return <Card 
                        name={scholar.name} 
                        text={scholar.text}  
                        range={scholar.range}  
                        image={scholar.image}
                        key={index}/>
                    })}
                </div>
            </div>
            
        </div>
    )
}

export default OurSchorlars
