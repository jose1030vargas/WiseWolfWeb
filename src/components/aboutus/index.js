import {useContext} from 'react'
import './index.css'
import {languages} from '../../languages/languages'
import Img from '../../assets/aboutImage.png'
import LangContext from '../../context/languages/LangContext'

const AboutUs = () => {
    const {language} = useContext(LangContext);
    return (
        <div id='about-cont' className='about-cont'>
            <div className='about-cont-img'>
                <div className='about-cont-text'>
                    <div id='about' className='about-textAll text-about'>
                        <h2 className='aboutus-titles-pad'>
                            {languages[language].title_aboutus}
                        </h2>
                        <p>
                            {languages[language].text_aboutus}
                        </p>
                    </div>
                    <div className='about-textAll text-mision'>
                        <h2 className='aboutus-titles-pad'>
                            {languages[language].title_mision}
                        </h2>
                        <p>
                            {languages[language].text_mision}
                        </p>
                    </div>
                    <div className='img-about'>
                        <img src={Img} alt='nft' />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AboutUs
