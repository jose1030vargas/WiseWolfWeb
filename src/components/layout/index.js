import React from 'react'
import MenuBar from '../menuBar'
import Footer from '../footer'
import ButtonTop from '../buttonTop'


const Layout = ({children}) => {

    return (
        <>
            <MenuBar/>
            
            {children}
            <Footer/>
            <ButtonTop />
        </>
    )
}

export default Layout
