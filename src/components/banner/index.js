import React from 'react'
import './index.css'
import Img from '../../assets/Banner1.png'
import Img2 from '../../assets/Banner2.png'

const Banner = ({type}) => {
    return (
        <div className='cont-banner'>
            <img className='img-banner' src={type === 1 ? Img : Img2} alt='Banner' />
        </div>
    )
}

export default Banner
