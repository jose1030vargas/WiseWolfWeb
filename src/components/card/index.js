import React from 'react'
import Avatar1 from '../../assets/avatar1.png'
import './index.css'


const Card = ({text='hola',name='eaas',range='asd',image=Avatar1}) => {
    return (
        <div className='cont-card'>
            <div><img className='card-img' src={image} alt='Scholar avatar' /></div>
            <div className='cont-card-text'><p className='card-text'>{text}</p></div>
            <h4>{name}</h4>
            <h5>{range}</h5>
        </div>
    )
}

export default Card
