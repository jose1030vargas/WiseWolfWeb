import {useContext} from 'react'
import LogoWise from '../../../assets/WiseWolf_PNG_web.png'
import AxieImg from '../../../assets/AxieImg.png'
import LogoAxie from '../../../assets/LogoAxie.png'
import Discord from '../../../assets/discord.png'
import Instagram from '../../../assets/instagram.png'
import Twitter from '../../../assets/twitter.png'
import AxieBird from '../../../assets/bird.gif'
import {languages} from '../../../languages/languages'
import LangContext from '../../../context/languages/LangContext'

import './index.css'


const FooterCont = () => {
    const {language} = useContext(LangContext);

    return (
        <div className='footer-contAll'>
            <img className='footer-gif' src={AxieBird} alt='Axie Bird' />
            <div className='footer-cont'>

                <div className='footer-cont-logo'>
                    <img className='footer-logo-wise' src={LogoWise} alt='Logo WiseWolf' />
                    <img className='footer-logo-axie' src={LogoAxie} alt='Logo Axie' />
                </div>

                <div className='footer-cont-wwa'>
                    <h2>Wise Wolf Academy</h2>
                    <h3>{language && languages[language].footer_pack}</h3>
                </div>

                <div className='footer-cont-netw'>
                    <h3>{language && languages[language].footer_socialNetw}</h3>
                    <div className='footer-icons'>
                        <a href='https://discord.gg/4k2RCZ4XB8' target="_blank" rel='noreferrer' ><img className='footer-icons-netw' src={Discord} alt='disc' /></a>
                        <a href='https://www.instagram.com/wisewolf_acad' target="_blank" rel='noreferrer' ><img  className='footer-icons-netw' src={Instagram} alt='inst' /></a>
                        <a href='https://twitter.com/WiseWolfAcad/' target="_blank" rel='noreferrer' ><img className='footer-icons-netw' src={Twitter} alt='twitt' /></a>
                    </div>
                </div>

                <div className='footer-cont-axie'>
                    <img src={AxieImg} alt='Axie' />
                </div>

            </div>
        </div>
    )
}

export default FooterCont
