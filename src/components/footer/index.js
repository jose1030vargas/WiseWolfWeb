
import FooterCont from './footerCont'
import './index.css'


const Footer = () => {
    return (
        <div className='cont-footer'>
            <div className='footer-svg'>
            <svg  viewBox="0 0 676 61" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 42.7001H28.1667C56.3333 42.7001 112.667 42.7001 169 37.6104C225.333 32.597 281.667 22.3032 338 18.3001C394.333 14.297 450.667 16.2032 507 15.2501C563.333 14.297 619.667 10.1032 647.833 8.13979L676 6.1001V61.0001H647.833C619.667 61.0001 563.333 61.0001 507 61.0001C450.667 61.0001 394.333 61.0001 338 61.0001C281.667 61.0001 225.333 61.0001 169 61.0001C112.667 61.0001 56.3333 61.0001 28.1667 61.0001H0V42.7001Z" fill="#00ACD0"/>
            </svg>

            </div>
            <div className='footer-inf'>
                <FooterCont />
                <div className='footer-copy'>
                    <h5 className='text-copy'>Copyright  <span className='symbol-copy'>&copy;</span>  2021   Wise Wolf Academy</h5>
                </div>
            </div>
        </div>
    )
}

export default Footer
