import { useReducer } from "react";
import LangContext from "./LangContext";
import reducer from "./LangReducer";

let langSelecc = localStorage.getItem("langSelecc") ?
localStorage.getItem("langSelecc") : 'en';



const LangState = ({ children }) => {
  const [language, dispatch] = useReducer(reducer, langSelecc);

  localStorage.setItem("langSelecc", 'en');

  return (
    <LangContext.Provider
      value={{
        language,
        dispatch,
      }}
    >
      {children}
    </LangContext.Provider>
  );
};

export default LangState;