import { languageScholar } from "../../languages/languages"
import {useState,useEffect} from 'react'
import Avatar1 from '../../assets/avatar1.png'
import ScholarGrabiel from '../../assets/ScholarGrabiel.png'
import ScholarMayi from '../../assets/ScholarMayi.png'
import ScholarCrimi from '../../assets/ScholarCrimi.png'
import ScholarJose from '../../assets/ScholarJose.png'
import ScholarMarie from '../../assets/ScholarMarie.png'
import ScholarSasse from '../../assets/ScholarSasse.png'
import ScholarHugo from '../../assets/ScholarHugo.png'


const imgScholar1 = ScholarGrabiel;
const imgScholar2 = ScholarMayi;
const imgScholar3 = ScholarCrimi;
const imgScholar4 = ScholarJose;
const imgScholar5 = ScholarMarie;
const imgScholar6 = ScholarSasse;
const imgScholar7 = ScholarHugo;


const useInfoCard = (language) => {

    const [scholars,setScholars] = useState([]);
    const [scholarsAll,setScholarsAll] = useState([]);

    useEffect(()=>{

        if(language){
            setScholarsAll(
            [
         
                {
                    text: language && languageScholar[language].scholar1_text,
                    range: language && languageScholar[language].scholar1_range,
                    image: imgScholar1 || Avatar1,
                    name: language && languageScholar[language].scholar1_name,
  
                },
                {
                    text: language && languageScholar[language].scholar2_text,
                    range: language && languageScholar[language].scholar2_range,
                    image: imgScholar2 || Avatar1,
                    name: language && languageScholar[language].scholar2_name,
    
                },{

                    text: language && languageScholar[language].scholar3_text,
                    range: language && languageScholar[language].scholar3_range,
                    image: imgScholar3 || Avatar1,
                    name: language && languageScholar[language].scholar3_name,
               
                },{
       
                    text: language && languageScholar[language].scholar4_text,
                    range: language && languageScholar[language].scholar4_range,
                    image: imgScholar4 || Avatar1,
                    name: language && languageScholar[language].scholar4_name,
                    
                },{
                   
                    text: language && languageScholar[language].scholar5_text,
                    range: language && languageScholar[language].scholar5_range,
                    image: imgScholar5 || Avatar1,
                    name: language && languageScholar[language].scholar5_name,
                  
                },{
                   
                    text: language && languageScholar[language].scholar6_text,
                    range: language && languageScholar[language].scholar6_range,
                    image: imgScholar6 || Avatar1,
                    name: language && languageScholar[language].scholar6_name,
                
                }
                ,{
                   
                    text: language && languageScholar[language].scholar7_text,
                    range: language && languageScholar[language].scholar7_range,
                    image: imgScholar7 || Avatar1,
                    name: language && languageScholar[language].scholar7_name,
                
                }
            ]
        )

        }
        
    },[language])

    useEffect(()=>{
        if(scholarsAll.length !== 0){

            let n = 0;
            let number;
            let one = null;
            let two = null;
            let three = null;
            do {
                number = Math.floor((Math.random() * 7) + 0);
                if ((number !== one) && (number !== two) && (number !== three)) {
                    n++;
                    if (n === 1) {
                        one = number;
                    }
                    if (n === 2) {
                        two = number;
                    }
                    if (n === 3) {
                        three = number;
                    }
                }
            } 
        while (n < 3);

        setScholars([scholarsAll[one],scholarsAll[two],scholarsAll[three]])
             
        }
    },[scholarsAll])

    return scholars;
   
}

export default useInfoCard
